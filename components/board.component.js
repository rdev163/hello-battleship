"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var BattleBoardComponent = (function () {
    function BattleBoardComponent() {
        this.gridSize = 10;
        this.placement = false;
        this.pristine = true;
        for (var i = 0; i < this.gridSize; i++) {
            this.rows.push(i);
            this.cols.push(i);
        }
        ;
        this.initGameBoard();
    }
    ;
    BattleBoardComponent.prototype.initGameBoard = function () {
        for (var i = 0; i < this.gridSize; i++) {
            for (var j = 0; j < this.gridSize; j++) {
            }
        }
    };
    BattleBoardComponent.prototype.setGameBoard = function () {
    };
    BattleBoardComponent.prototype.cellClicked = function () {
        if (this.pristine) {
            this.placement = true;
            this.pristine = false;
        }
    };
    BattleBoardComponent.prototype.getBGColor = function () {
        if (this.pristine) {
            return 'grey';
        }
        else if (this.placement) {
            return 'blue';
        }
    };
    BattleBoardComponent = __decorate([
        core_1.Component({
            selector: 'battle-board',
            template: "\n<div class=\"text-center\" *ngFor=\"let row of rows\">\n {{row}}\n    <span *ngFor=\"let col of cols\">\n      <span [style.background-color]=\"getBGColor()\" (click)=\"cellClicked()\" class=\"cell\">[ ]</span>\n    </span>\n  </div>\n"
        }), 
        __metadata('design:paramtypes', [])
    ], BattleBoardComponent);
    return BattleBoardComponent;
}());
exports.BattleBoardComponent = BattleBoardComponent;
//# sourceMappingURL=board.component.js.map