import {Component} from '@angular/core';

@Component({
  selector: 'board-cell',
  template: `
  <span [style.background-color]="getBGColor()" (click)="cellClicked()" class="cell">[ ]</span>
`,
  styles: [`
 
`]
})

export class BoardCellCompopnent {
  public placement: boolean = false;
  public damaged: boolean = false;
  public miss: boolean = false;
  public pristine: boolean = true;

  constructor(){

  }

  public cellClicked(): void {
    if (this.pristine) {
      this.placement = true;
      this.pristine = false;
    }
  }

  public getBGColor(): string {
    if (this.pristine) {
      return 'grey';
    } else if (this.placement) {
      return 'blue';
    }
  }
}
