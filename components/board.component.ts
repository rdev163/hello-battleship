import {Component} from '@angular/core';

@Component({
  selector: 'battle-board',
  template: `
<div class="text-center" *ngFor="let row of rows">
 {{row}}
    <span *ngFor="let col of cols">
      <span [style.background-color]="getBGColor()" (click)="cellClicked()" class="cell">[ ]</span>
    </span>
  </div>
`
})

export class BattleBoardComponent {
  public gridSize: number = 10;
  public playerBoard: any[];
  public opponentBoard: any[];
  public cell: {
    disposition: string;
  };
  public placement: boolean = false;
  public pristine: boolean = true;
  public rows: any[];
  public cols: any[];


  constructor(){
    for (let i = 0; i < this.gridSize; i++) {
      this.rows.push(i);
      this.cols.push(i);
    };
    this.initGameBoard();

  };

  public initGameBoard(): void {
    for (let i = 0; i < this.gridSize; i++) {
      for (let j = 0; j < this.gridSize; j++) {

      }
    }
  }

  public setGameBoard(): any {

  }


  public cellClicked(): void {
    if (this.pristine) {
      this.placement = true;
      this.pristine = false;
    }
  }

  public getBGColor(): string {
    if (this.pristine) {
      return 'grey';
    } else if (this.placement) {
      return 'blue';
    }
  }
}
