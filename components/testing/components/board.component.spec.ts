
import {TestBed, ComponentFixture, async} from "@angular/core/testing";
import {BattleBoardComponent} from "../../board.component";
import {DebugElement} from "@angular/core";
import { By }           from '@angular/platform-browser';


TestBed.configureTestingModule({
  declarations: [BattleBoardComponent]
});

describe('BattleBoardComponent', () => {
  let de: DebugElement;
  let comp: BattleBoardComponent;
  let fixture: ComponentFixture<BattleBoardComponent>;
  let el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BattleBoardComponent]
    })
      .compileComponents()
      .then(() => {
      fixture = TestBed.createComponent(BattleBoardComponent);
      comp = fixture.componentInstance;
      de = fixture.debugElement.query(By.css('span'));
      el = de.nativeElement;

      })
  }))
})
