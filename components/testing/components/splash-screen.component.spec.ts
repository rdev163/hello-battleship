import {DebugElement} from "@angular/core";
import {SplashScreenComponent} from "../../splash-screen.component";
import {async, TestBed, ComponentFixture, tick, fakeAsync, inject} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {rootRoute} from "@angular/router/src/router_module";
import {StartMenuComponent} from "../../start-menu.component";
import {Router, ActivatedRoute} from "@angular/router";
import {ActivatedRouteStub} from "../stubs/router-stubs";

describe('SpalshScreenComponent', () => {
  let titleDE: DebugElement;
  let authorDE: DebugElement;
  let comp: SplashScreenComponent;
  let fixture: ComponentFixture<SplashScreenComponent>;
  let titleEl: HTMLElement;
  let authorEl: HTMLElement;




  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SplashScreenComponent,
        StartMenuComponent
      ],
      providers: [
        { provide: Router, useClass: RouterStub},
        { provide: ActivatedRoute, useClass: ActivatedRouteStub },
      ]
    })
      .compileComponents().then(() => {
      fixture = TestBed.createComponent(SplashScreenComponent);
      comp = fixture.componentInstance;
      titleDE = fixture.debugElement.query(By.css('h1'));
      authorDE = fixture.debugElement.query(By.css('small'));
      titleEl = titleDE.nativeElement;
      authorEl = authorDE.nativeElement;

    })
  }));

  it('should create component', () => expect(comp).toBeDefined() );

  it('should have a title', () => {
    fixture.detectChanges();
    const h1 = titleDE.nativeElement;
    expect(h1.innerText).toMatch('Hello BattleShip');
  });

  it('should have an author attribution', () => {
    fixture.detectChanges();
    const small = authorDE.nativeElement;
    expect(small.innerText).toMatch('A game by Raven/0');
  });

  it('should change route to StartMenuComponent in 4 seconds (fakeAsync)', fakeAsync(() => {
      inject([Router], (router: Router) => {

        const spy = spyOn(router, 'navi')

        tick(5000);
        expect(rootRoute.arguments).toMatch('start-menu');
      })
  }));
});

class RouterStub {
  navigateByUrl(url: string) {return url;}
}
