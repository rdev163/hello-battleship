"use strict";
var splash_screen_component_1 = require("../../splash-screen.component.ts");
var testing_1 = require("@angular/core/testing");
var platform_browser_1 = require("@angular/platform-browser");
var router_module_1 = require("@angular/router/src/router_module");
var start_menu_component_1 = require("../../start-menu.component");
var router_1 = require("@angular/router");
var router_stubs_1 = require("../stubs/router-stubs");
describe('SpalshScreenComponent', function () {
    var titleDE;
    var authorDE;
    var comp;
    var fixture;
    var titleEl;
    var authorEl;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [
                splash_screen_component_1.SplashScreenComponent,
                start_menu_component_1.StartMenuComponent
            ],
            providers: [
                { provide: router_1.Router, useClass: RouterStub },
                { provide: router_1.ActivatedRoute, useClass: router_stubs_1.ActivatedRouteStub },
            ]
        })
            .compileComponents().then(function () {
            fixture = testing_1.TestBed.createComponent(splash_screen_component_1.SplashScreenComponent);
            comp = fixture.componentInstance;
            titleDE = fixture.debugElement.query(platform_browser_1.By.css('h1'));
            authorDE = fixture.debugElement.query(platform_browser_1.By.css('small'));
            titleEl = titleDE.nativeElement;
            authorEl = authorDE.nativeElement;
        });
    }));
    it('should create component', function () { return expect(comp).toBeDefined(); });
    it('should have a title', function () {
        fixture.detectChanges();
        var h1 = titleDE.nativeElement;
        expect(h1.innerText).toMatch('Hello BattleShip');
    });
    it('should have an author attribution', function () {
        fixture.detectChanges();
        var small = authorDE.nativeElement;
        expect(small.innerText).toMatch('A game by Raven/0');
    });
    it('should change route to StartMenuComponent in 4 seconds (fakeAsync)', testing_1.fakeAsync(function () {
        testing_1.inject([router_1.Router], function (router) {
            var spy = spyOn(router, 'navi');
            testing_1.tick(5000);
            expect(router_module_1.rootRoute.arguments).toMatch('start-menu');
        });
    }));
});
var RouterStub = (function () {
    function RouterStub() {
    }
    RouterStub.prototype.navigateByUrl = function (url) { return url; };
    return RouterStub;
}());
//# sourceMappingURL=splash-screen.component.spec.js.map
