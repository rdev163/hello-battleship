import {Component, OnInit} from "@angular/core";

@Component({
  selector: 'splash-screen',
  template: `
    <h1>{{splashTitle}}</h1>
    
    <small>{{author}}</small>
    
`
})

export class SplashScreenComponent implements OnInit {
  public splashTitle: string;
  public author: string;

  ngOnInit() {

    this.splashTitle = 'Hello BattleShip';
    this.author = 'A game by Raven/0';

  }
}
