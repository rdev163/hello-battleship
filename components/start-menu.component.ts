import {Component, OnInit} from "@angular/core";

@Component({
  template: `
  <h1>{{startMenuTitle}}</h1>
  
  Input player name: <input type="text"/>
`
})

export class StartMenuComponent implements OnInit {
  public startMenuTitle: string;

  ngOnInit() {
    this.startMenuTitle = 'Start Menu'
  }
}
