"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var BoardCellCompopnent = (function () {
    function BoardCellCompopnent() {
        this.placement = false;
        this.damaged = false;
        this.miss = false;
        this.pristine = true;
    }
    BoardCellCompopnent.prototype.cellClicked = function () {
        if (this.pristine) {
            this.placement = true;
            this.pristine = false;
        }
    };
    BoardCellCompopnent.prototype.getBGColor = function () {
        if (this.pristine) {
            return 'grey';
        }
        else if (this.placement) {
            return 'blue';
        }
    };
    return BoardCellCompopnent;
}());
BoardCellCompopnent = __decorate([
    core_1.Component({
        selector: 'board-cell',
        template: "\n  <span [style.background-color]=\"getBGColor()\" (click)=\"cellClicked()\" class=\"cell\">[ ]</span>\n",
        styles: ["\n \n"]
    }),
    __metadata("design:paramtypes", [])
], BoardCellCompopnent);
exports.BoardCellCompopnent = BoardCellCompopnent;
//# sourceMappingURL=board-celll.component.js.map