"use strict";
var testing_1 = require("@angular/core/testing");
var board_component_1 = require("../../board.component");
var platform_browser_1 = require("@angular/platform-browser");
testing_1.TestBed.configureTestingModule({
    declarations: [board_component_1.BattleBoardComponent]
});
describe('BattleBoardComponent', function () {
    var de;
    var comp;
    var fixture;
    var el;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [board_component_1.BattleBoardComponent]
        })
            .compileComponents()
            .then(function () {
            fixture = testing_1.TestBed.createComponent(board_component_1.BattleBoardComponent);
            comp = fixture.componentInstance;
            de = fixture.debugElement.query(platform_browser_1.By.css('span'));
            el = de.nativeElement;
        });
    }));
});
//# sourceMappingURL=board.component.spec.js.map