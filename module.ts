import { NgModule, CUSTOM_ELEMENTS_SCHEMA }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent }  from './components/app.component';
import  {BattleBoardComponent} from './components/board.component';
import  {BoardCellCompopnent} from './components/board-celll.component';
import {Router, ActivatedRoute} from "@angular/router";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    Router,
    ActivatedRoute
  ],
  declarations: [
    AppComponent,
    BattleBoardComponent,
    BoardCellCompopnent
  ],
  bootstrap: [ AppComponent ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class AppModule { }
